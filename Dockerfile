FROM ruby:2.4.2
RUN apt-get update && apt-get -y install nodejs

RUN mkdir /app
WORKDIR /app

ADD Gemfile /app
RUN bundle install

CMD ["bash"]
