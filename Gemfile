# coding: utf-8
source 'https://gems.ruby-china.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.0', '>= 5.0.0.1'
# Use mysql as the database for Active Record
gem 'mysql2', '>= 0.3.18', '< 0.5'

gem 'puma', '~> 3.10'

gem "audited", "~> 4.5"
gem 'annotate', '~> 2.7'

gem 'newrelic_rpm'

# unicorn-rails is a simple gem that sets the default server for rack (and rails) to unicorn.
# https://github.com/samuelkadolph/unicorn-rails
gem 'unicorn-rails', '~> 2.2.1'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Makes http fun again!
# https://github.com/jnunemaker/httparty
gem 'httparty'

# Official Sass port of Bootstrap 2 and 3.
# https://github.com/twbs/bootstrap-sass
gem 'bootstrap-sass', '~> 3.3.7'

# the font-awesome font bundled as an asset for the rails asset pipeline
# http://fortawesome.github.com/Font-Awesome/
gem 'font-awesome-rails', '~> 4.7'

# An opinionated micro-framework for creating REST-like APIs in Ruby.
# https://github.com/ruby-grape/grape
gem 'grape', '~> 0.18.0'

# Add auto generated documentation to your Grape API that can be displayed with Swagger.
# https://github.com/ruby-grape/grape-swagger
gem 'grape-swagger', '~> 0.25.0'

# Swagger UI as Rails Engine for grape-swagger gem.
# https://github.com/ruby-grape/grape-swagger-rails
gem 'grape-swagger-rails', '~> 0.3.0'

# Entities - a simple Facade to use with your models and API - extracted from Grape.
# https://github.com/ruby-grape/grape-entity
gem 'grape-entity', '~> 0.5.1'

# A fast, safe and extensible Markdown to (X)HTML parser
# We use it to support markdown in notes of grape-swagger
# http://github.com/vmg/redcarpet
gem 'redcarpet', '~> 3.3.4'

# Rouge aims to a be a simple, easy-to-extend drop-in replacement for pygments
# We use it to support highlight codes in notes of grape-swagger
# http://github.com/jneen/rouge
gem 'rouge', '~> 2.0.6'

# Ruby bindings for the PingPlusPlus API
# https://github.com/PingPlusPlus/pingpp-ruby
gem 'pingpp', '~> 2.0', '>= 2.0.15'

# WeChat gem tries to help Rails developer to integrate enterprise account / public account easily.
# https://github.com/Eric-Guo/wechat
gem 'wechat', '~> 0.8.3'

# Flexible authentication solution for Rails with Warden
# https://github.com/plataformatec/devise
gem 'devise', '~> 4.2.0'

# Rack::Cors provides support for Cross-Origin Resource Sharing (CORS) for Rack compatible web applications.
# https://github.com/cyu/rack-cors
gem 'rack-cors', :require => 'rack/cors'

# Inherited Resources speeds up development by making your controllers inherit all restful actions so you just have to focus on what is important.
# http://github.com/josevalim/inherited_resources
# use github to support rails 5
# gem 'inherited_resources', github: 'activeadmin/inherited_resources'

# The administration framework for Ruby on Rails applications.
# https://github.com/activeadmin/activeadmin
gem 'activeadmin', '~> 1.0.0'

# flat skin for activeadmin
# https://github.com/activeadmin-plugins/active_admin_theme
gem 'active_admin_theme'

# Set of addons to help with the activeadmin ui
# https://github.com/platanus/activeadmin_addons
gem 'activeadmin_addons', '~> 0.9.3'

# This gem adds storage support for Qiniu to Carrierwave example: https://github.com/huobazi/carrierwave-qiniu-example
# https://github.com/huobazi/carrierwave-qiniu
gem 'carrierwave-qiniu', '~> 1.0.0'

# A tagging plugin for Rails applications that allows for custom tagging along dynamic contexts
# https://github.com/mbleigh/acts-as-taggable-on
gem 'acts-as-taggable-on', '~> 4.0'

# State machines make it dead-simple to manage the behavior of a class
# https://github.com/pluginaweek/state_machine
gem 'aasm', '~> 4.11.1'

# for sending notifications when errors occur
# https://github.com/smartinez87/exception_notification
gem 'exception_notification', '~> 4.1', '>= 4.1.4'

# Faster Faker, generates dummy data.
# https://github.com/stympy/faker
gem 'faker', '~> 1.6.6', require: false

# 阿里大鱼发送短信ruby sdk
# https://github.com/davidqhr/alidayu-ruby
gem 'alidayu-ruby', require: 'alidayu'

# The (admittedly crazy) goal of this Gem is to be able to normalize/format/split all phone numbers in the world.
# https://github.com/floere/phony
gem 'phony', '~> 2.15.20'

# Sequel: The Database Toolkit for Ruby
# https://github.com/jeremyevans/sequel
gem 'sequel', '~> 4.40.0', require: false

# a gem to help you select chinese area like province, city and district
# https://github.com/saberma/china_city
gem 'china_city', '~> 0.0.4'

# Simple, efficient background processing for Ruby.
# https://github.com/mperham/sidekiq
gem 'sidekiq', '< 6'
gem 'sidetiq', '~> 0.7.2'

# Use Redis adapter to run Action Cable in production
# https://github.com/redis/redis-rb
gem 'redis', '~>3.2'

# Adds a Redis::Namespace class which can be used to namespace Redis keys
# https://github.com/resque/redis-namespace
gem 'redis-namespace', '~> 1.5'

# CanCan is an authorization library for Ruby on Rails which restricts what resources a given user is allowed to access
# https://github.com/CanCanCommunity/cancancan
gem 'cancancan', '~> 1.14.0'

# This gem provides xls downloads for Active Admin resources.
# https://github.com/beansmile/activeadmin-xls
gem 'activeadmin-xls', github: 'beansmile/activeadmin-xls'

# Create beautiful JavaScript charts with one line of Ruby
# https://github.com/ankane/chartkick
gem "chartkick", '~> 2.2.1'

# Helps you to convert Chinese characters into pinyin form.
# https://github.com/janx/ruby-pinyin
gem 'ruby-pinyin', '~> 0.5'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  # gem 'spring'
  # gem 'spring-watcher-listen', '~> 2.0.0'

  # http://capistranorb.com/
  # Capistrano is a utility and framework for executing commands in parallel on multiple remote machines, via SSH.
  # https://github.com/capistrano/capistrano
  gem 'capistrano', '~> 3.6.1', require: false

  # Quickly troubleshoot capistrano issues related to SSH
  # https://github.com/capistrano-plugins/capistrano-ssh-doctor
  gem 'capistrano-ssh-doctor', '~> 1.0'
  # RVM integration for Capistrano
  # https://github.com/capistrano/rvm
  gem 'capistrano-rvm', '~> 0.1.2', require: false

  # Bundler support for Capistrano 3.x
  # https://github.com/capistrano/bundler
  gem 'capistrano-bundler', '~> 1.2.0', require: false

  # Rails specific Capistrano tasks
  # https://github.com/capistrano/rails
  gem 'capistrano-rails', '~> 1.2.0', require: false

  # Capistrano plugin which adds a remote rails console
  # https://github.com/ydkn/capistrano-rails-console
  gem 'capistrano-rails-console', '~> 2.2.0', require: false

  # Run any rake task on a remote server using Capistrano
  # https://github.com/sheharyarn/capistrano-rake
  gem 'capistrano-rake', '~> 0.1.0', require: false

  # A collection of capistrano tasks for syncing assets and databases
  # https://github.com/sgruhier/capistrano-db-tasks
  gem 'capistrano-db-tasks', '~> 0.5', require: false

  # Sidekiq integration for Capistrano
  # https://github.com/seuros/capistrano-sidekiq
  gem 'capistrano-sidekiq', '~> 0.5.4', require: false

  # Unicorn specific Capistrano tasks
  # https://github.com/tablexi/capistrano3-unicorn
  gem 'capistrano3-unicorn', '~> 0.2.0', require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

group :development do
  gem 'pry-rails', '~> 0.3.4'
  gem 'pry-byebug', '~> 3.4'
end
